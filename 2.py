import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer

import nltk
from os import listdir
from os.path import isfile

file_list = []

pattern = "PORTIA"


def get_files():
    for file in listdir("D:/corpus"):

        if file.endswith(".txt"):
            file_list.append(file)


def search_pattern():
    file_content = open("D:/corpus/play1.txt").read()
    tokens = nltk.word_tokenize(file_content)

    stemmer = nltk.stem.PorterStemmer()

    vec = CountVectorizer()
    X = vec.fit_transform(stemmer.stem(token) for token in tokens)
    df = pd.DataFrame(X.toarray(), columns=vec.get_feature_names())
    df.to_csv('TDIM.csv')


def scan_files():
    for file in file_list:
        search_pattern(file)


if __name__ == '__main__':
    search_pattern()
