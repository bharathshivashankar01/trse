from os import listdir
from collections import defaultdict

file_list = []
totalword = []
directory = '/home/bharath/PycharmProjects/TRSE/corpus/'


def get_files():
    for file in listdir(directory):

        if file.endswith(".txt"):
            file_list.append(file)
    return file_list


def get_words():
    for file in file_list:
        with open(directory + file) as f:
            words = f.read().split()
        totalword.append(set(words))
    print(totalword)


def invertedIndex():
    inv_indx = defaultdict(list)
    for idx, text in enumerate(totalword):
        for word in text:
            inv_indx[word].append(idx)
    print(inv_indx)


if __name__ == '__main__':
    get_files()
    get_words()
    invertedIndex()
