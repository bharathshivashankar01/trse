import nltk
from os import listdir
from os.path import isfile

file_list = []

pattern_one = "PORTIA"
pattern_two = "SCENE"
pattern_three = "Julius"

pattern_one_list = []
pattern_two_list = []
pattern_three_list = []


def get_files():
    for file in listdir("D:/corpus"):

        if file.endswith(".txt"):
            file_list.append(file)


def search_pattern(file, pattern):
    file_content = open("D:/corpus/" + file).read()
    tokens = nltk.word_tokenize(file_content)

    if pattern in tokens:
        return True


def scan_files(number, pattern):
    for file in file_list:

        if number == 1:

            if search_pattern(file, pattern): pattern_one_list.append(file)

        elif number == 2:

            if search_pattern(file, pattern): pattern_two_list.append(file)

        elif number == 3:

            if search_pattern(file, pattern): pattern_three_list.append(file)


if __name__ == '__main__':
    get_files()
    scan_files(1, pattern_one)
    scan_files(2, pattern_two)
    scan_files(3, pattern_three)

    inverted_dict.update(PORTIA=pattern_one_list)
    inverted_dict.update(SCENE=pattern_two_list)
    inverted_dict.update(Julius=pattern_three_list)

    print(inverted_dict["PORTIA"])
    print(inverted_dict["SCENE"])
    print(inverted_dict["Julius"])