import os
import sys
import re
import pandas as pd
import numpy as np

corpus = list of files

text1 = []
for fname in corpus:
    with open(fname,'r') as file:
        text1.append(file.read())


dataset['corpus'] = corpus

dataset['text'] = text1

dataset.head()

import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer

vec = CountVectorizer()
x = vec.fit_transform(dataset.iloc[:,1])
df = pd.DataFrame(x.toarray(), columns=vec.get_feature_names())
print(df)