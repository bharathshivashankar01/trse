import nltk
from os import listdir

file_list = []
total_files = []


def get_files(directory):
    for file in listdir(directory):

        if file.endswith(".txt"):
            file_list.append(file)
    return file_list


# def search_pattern(file, pattern, directory):
#     with open(directory + file) as openfile:
#
#         for line in openfile:
#
#             for word in line.split(','):
#
#                 if pattern in word:
#                     return True


def search_pattern(file, pattern, directory):
    with open(directory + file) as openfile:

        for line in openfile:

            for word in line.split(','):

                if pattern in word:
                    return True


# def search_pattern(file, pattern, directory):
#     file_content = open(directory + file).read()
#     tokens = nltk.word_tokenize(file_content)
#
#     if pattern in tokens:
#         return True


def scan_files(directory, pattern):
    for pattern in pattern.split(' '):
        for file in file_list:

            if search_pattern(file, pattern, directory):
                l1 = []
                dict1 = {pattern: l1.append(file)}
                # dict(pattern,pattern.append(file))
                total_files.append(file)
    # return list(dict.fromkeys(total_files))
    return dict1

# def scan_files(directory, pattern):
#     for file in file_list:
#
#         if search_pattern(file, pattern, directory):
#             print(file)
