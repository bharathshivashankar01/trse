import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer


def term_document_incidence_matrix(documents):
    vec = CountVectorizer()
    X = vec.fit_transform(documents)
    df = pd.DataFrame(X.toarray(), columns=vec.get_feature_names())
    print(df)
