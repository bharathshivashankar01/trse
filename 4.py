import nltk

from nltk.corpus import stopwords

def search_pattern():
    file_content = open("D:/corpus/play1.txt").read()

    word_punct_tokenizer = nltk.tokenize.WordPunctTokenizer()

    # TOKENIZATION
    token_words = word_punct_tokenizer.tokenize(file_content)

    copy_token_words = token_words

    for token in token_words:

        if token in stopwords.words('english'):
            copy_token_words.remove(token)

    # print(copy_token_words)

    # CAPITALIZATION/CASE FOLDING

    # STEMMING

    stem = nltk.stem.PorterStemmer()

    copy_token_words = [stem.stem(word) for word in copy_token_words]
    # print(copy_token_words)

    copy_token_words = [word.capitalize() for word in copy_token_words]

    print(copy_token_words)


if __name__ == '__main__':
    search_pattern()