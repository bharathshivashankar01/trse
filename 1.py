import nltk
from os import listdir
from os.path import isfile

file_list = []

pattern = "PORTIA"


def get_files():
    for file in listdir("D:/corpus"):

        if file.endswith(".txt"):
            file_list.append(file)


def search_pattern(file):
    file_content = open("D:/corpus/" + file).read()
    tokens = nltk.word_tokenize(file_content)

    if pattern in tokens:
        return True


def scan_files():
    for file in file_list:

        if search_pattern(file):
            print(file)


if __name__ == '__main__':
    get_files()
    scan_files()
